#!/usr/bin/python3
from flask import Flask
import subprocess, re, json
app = Flask(__name__)
    
# this is hacky but it works

@app.route("/bw")
def return_bw_stats_json():
    # raw output of 'ipfs stats bw'
    bwStatsRaw = subprocess.run("$GOPATH/bin/ipfs stats bw", shell=True, stdout=subprocess.PIPE).stdout
    stringOut = bwStatsRaw.decode("utf-8")
    
    # bandwidth stats regex
    bwRe = re.compile(r"(\S*): ([\d]+[\D\d]*?\s[B|kB|MB|GB|TB|PB]+[/s]*)")
    bwReMatches = re.findall(bwRe, stringOut)

    # crafted JSON string
    stringFormatted = "bw: {" + '"{}":"{}","{}":"{}","{}":"{}","{}":"{}"'.format(bwReMatches[0][0], bwReMatches[0][1], bwReMatches[1][0], bwReMatches[1][1], bwReMatches[2][0], bwReMatches[2][1], bwReMatches[3][0], bwReMatches[3][1]) + "}"
    return stringFormatted

@app.route("/repo")
def return_repo_stats_json():
    # raw output of 'ipfs stats repo'
    repoStatsRaw = subprocess.run("$GOPATH/bin/ipfs stats repo", shell=True, stdout=subprocess.PIPE).stdout
    stringOut = repoStatsRaw.decode("utf-8")

    # repo stats regex
    repoRe = re.compile(r"(\S*): +(\/*\w+\-*\/*\w*\@*\/*\.*\w*)")
    repoReMatches = re.findall(repoRe, stringOut)

    # crafted JSON string
    stringFormatted = "repo: {" + '"{}":"{}","{}":"{}","{}":"{}","{}":"{}"'.format(repoReMatches[0][0], repoReMatches[0][1], repoReMatches[1][0], repoReMatches[1][1], repoReMatches[2][0], repoReMatches[2][1], repoReMatches[3][0], repoReMatches[3][1]) + "}"
    return stringFormatted

# not yet implemented
# @app.route("/bitswap")
# def return_bitswap_stats_json():
#     # raw output of 'ipfs stats bitswap'
#     bitswapStatsRaw = subprocess.run("$GOPATH/bin/ipfs stats bitswap", shell=True, stdout=subprocess.PIPE).stdout
#     stringOut = bitswapStatsRaw.decode("utf-8")

#     # bitswap stats regex
#     bitswapRe = re.compile(r"")
#     bitswapReMatches = re.findall(bitswapRe, stringOut)

#     # crafted JSON string
#     stringFormatted = "{" + '' + "}"
#     return stringFormatted
